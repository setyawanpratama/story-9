from django.test import TestCase, LiveServerTestCase, Client
from django.urls import resolve
from django.http import HttpRequest

from .views import *

class test_Unit(TestCase):
        def test_add_url_is_exist(self):
                response= Client().get('/')
                self.assertEqual(response.status_code,200)
                
        def test_template_exist(self):
                response= Client().get('/')
                self.assertTemplateUsed(response,"tampilan.html")

        def test_file_function(self):
                found = resolve("/")
                self.assertEqual(found.func, tampilan)

        def test_konten_halaman_ada_isi(self):
                self.assertIsNotNone(tampilan)

