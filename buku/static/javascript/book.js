const formatBook = (books) => {
    $('#hasil').append(   
	"<thead class='thead-light'>"+	
        "<tr>" +
            "<th>Title</th>" +
            "<th>Book Cover</th>" +
            "<th>Author(s)</th>" +
        "</tr>" +
	"</thead>"
    );
    for(i=0 ; i < books.items.length; i++){
        $('#hasil').append("<tr>");
        $('#hasil').append("<td><p>" + books.items[i].volumeInfo.title + "</p></td>");                    
        try{    
        $('#hasil').append("<td><img src='" + books.items[i].volumeInfo.imageLinks.thumbnail + "'></img></td>");
        }
        catch{
            $("#hasil").append("<td><p> No image </p></td>");                   
        }
        try{
            if(books.items[i].volumeInfo.authors.length > 0){
                $("#hasil").append(`
                <td>
                    ${books.items[i].volumeInfo.authors.map((obj) => {
                        return `${obj}`
                    }).join("")}
                </td>
                `)
            }
        }
        catch{
            $("#hasil").append("<td><p> No known authors </p></td>");  
        }
        $('#hasil').append("</tr>");
    }
}
$(document).ready(() => {

    $.ajax({
        method: 'GET',
        url: 'https://www.googleapis.com/books/v1/volumes?q=pendidikan',
        success: function(books){
            console.log(books);
            $('#hasil').empty();
            formatBook(books)
        }
    })

    $('#submit').on('click', function(){
        let key = $("#foundbook").val();
		
        $.ajax({
            method: 'GET',
            url: 'https://www.googleapis.com/books/v1/volumes?q=' + key,
            success: function(books){
                $('#hasil').empty();
                formatBook(books)
            }
        })
    })

})
